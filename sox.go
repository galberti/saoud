package main

import (
    "fmt"
    "runtime"
    _ "math"

    "github.com/krig/go-sox"
)

func rec_loop(aogchan chan<- *[]int16) {
    // wanted signal values
    si := sox.NewSignalInfo(44100, 2, 32, 0, nil)
    var i *sox.Format
    switch runtime.GOOS {
        case "darwin":
            i = sox.OpenRead0("default", si, nil, "coreaudio")
        case "linux":
            i = sox.OpenRead0("hw:1,1,1", si, nil, "alsa")
        default:
            i = nil
    }
    if i == nil {
        panic("Failed to open input dev")
    }
    fmt.Printf("Opened input audio device\n")
    fmt.Printf("Input channels: %d\n", i.Signal().Channels())
    fmt.Printf("Input rate: %.2f\n", i.Signal().Rate())

    oe := sox.NewEncodingInfo(sox.ENCODING_SIGN2, 32, 0, false)

    // 10 ms
    spl_card := int(uint(i.Signal().Rate()) / 100)
    spl_opus := 480
    if spl_card > spl_opus {
        panic("Implement general solution for soundcard")
    }
    sss32 := make([]sox.Sample, spl_opus*2)
    sss := make([]int16, spl_opus)

    ibuf := sox.NewMemstream()
    obuf := sox.NewMemstream()
    for {
        var r int64 = 0
        p := 0
        usplc2 := spl_card*2
        for {
            r = r + i.Read(sss32[p:], uint(usplc2-p))
            if int(r) == usplc2 {
                break
            }
            p = p + int(r)
        }
        // align si to obtained signal values
        si = i.Signal().Copy()
        inms := sox.OpenMemstreamWrite(ibuf, si, nil, "sox")
        if inms == nil {
            panic("Failed to open inms out memory buffer")
        }
        inms.Write(sss32, uint(spl_card)*2)
        inms.Release()

        inms = sox.OpenMemRead0(ibuf, si, nil, "sox")
        if inms == nil {
            panic("Failed to open inms in memory buffer")
        }

        chain := sox.CreateEffectsChain(oe, oe)

        // input doesn't modify input signal infos..
        e := sox.CreateEffect(sox.FindEffect("input"))
        e.Options(inms)
        chain.Add(e, si, si)
        e.Release()

        // ..other filters often do. I want same rate, 1 channel here
        so1 := sox.NewSignalInfo(si.Rate(), 1, 32, 0, nil)
        if (si.Channels() != so1.Channels()) {
            e = sox.CreateEffect(sox.FindEffect("channels"))
            e.Options()
            chain.Add(e, si, so1)
            e.Release()
        }

        // and if needed, adjust rate
        so2 := sox.NewSignalInfo(48000, 1, 32, 0, nil)
        if (so1.Rate() != so2.Rate()) {
            e = sox.CreateEffect(sox.FindEffect("rate"))
            e.Options()
            chain.Add(e, so1, so2)
            e.Release()
        }

        outms := sox.OpenMemstreamWrite(obuf, so2, nil, "sox")
        if outms == nil {
            panic("Failed to open outms out memory buffer")
        }
        e = sox.CreateEffect(sox.FindEffect("output"))
        e.Options(outms)
        chain.Add(e, so2, so2)
        e.Release()

        chain.Flow()

        chain.Release()
        inms.Release()
        outms.Release()

        outms = sox.OpenMemRead0(obuf, so2, nil, "sox")
        outms.Read(sss32, uint(spl_opus))
        outms.Release()
        for i := 0; i < spl_opus; i++ {
            sss[i] = int16(sss32[i] >> 16)
        }
        aogchan <- &sss
    }
}

func play_loop(s <-chan *[]int16) {
    // rate, channel, precision, length, mult
    so2 := sox.NewSignalInfo(44100, 2, 32, 0, nil)

    var o *sox.Format
    switch runtime.GOOS {
        case "darwin":
            o = sox.OpenWrite("default", so2, nil, "coreaudio")
        case "linux":
            o = sox.OpenWrite("hw:1,1,1", so2, nil, "alsa")
            // "hw:1,1,1"
        default:
            o = nil
    }
    if o == nil {
        panic("Failed to open output dev")
    }
    fmt.Printf("Opened output audio device\n")
    fmt.Printf("Output channels: %d\n", o.Signal().Channels())
    fmt.Printf("Output rate: %.2f\n", o.Signal().Rate())

    // encoding, bitpersample, compression, oppositeendian
    oe := sox.NewEncodingInfo(sox.ENCODING_SIGN2, 32, 0, false)

    buf := sox.NewMemstream()

    for {
        sample := <-s
        slen := len(*sample)
        sss := make([]sox.Sample, slen)
        for i, s := range *sample {
            // should I try to do the scaling in the chain?
            sss[i] = sox.Sample(s) << 16
        }
        // rate, channel, precision, length, mult
        si := sox.NewSignalInfo(48000, 1, 32, 0, nil)
        so2 = o.Signal().Copy()

        newin := sox.OpenMemstreamWrite(buf, si, nil, "sox")
        if newin == nil {
            panic("Failed to open memory buffer")
        }
        newin.Write(sss, uint(slen))
        newin.Release()

        newin = sox.OpenMemRead(buf)

        chain := sox.CreateEffectsChain(oe, oe)
        e := sox.CreateEffect(sox.FindEffect("input"))
        e.Options(newin)
        chain.Add(e, si, si)
        e.Release()

        // effects..
        so1 := sox.NewSignalInfo(so2.Rate(), 1, 32, 0, nil)
        if (si.Rate() != so2.Rate()) {
            e = sox.CreateEffect(sox.FindEffect("rate"))
            e.Options()
            chain.Add(e, si, so1)
            e.Release()
        }

        if (so1.Channels() != so2.Channels()) {
            e = sox.CreateEffect(sox.FindEffect("channels"))
            e.Options()
            // this modifies *also* so1 (wtf .. )
            chain.Add(e, so1, so2)
            e.Release()
        }

        e = sox.CreateEffect(sox.FindEffect("output"))
        e.Options(o)
        // meh
        chain.Add(e, o.Signal(), o.Signal())
        e.Release()

        chain.Flow()
        newin.Release()
        chain.Release()
    }
}

func sox_init() {
    if !sox.Init() {
        panic("Failed to initialize SoX")
    }
    if !sox.FormatInit() {
        panic("Failed to initialize SoX formats")
    }
}

