package svxlink

import (
    "fmt"
    "net"
    "os"
    "strconv"
    "sync"
    "time"

    "crypto/hmac"
    "crypto/sha1"
    "encoding/binary"
)

const (
    TYPE_HEARTBEAT  = 1
    TYPE_PROTOVER   = 5
    TYPE_AUTHCHAL   = 10
    TYPE_AUTHRESP   = 11
    TYPE_AUTHOK     = 12
    TYPE_ERROR      = 13
    TYPE_SERVINFO   = 100
    TYPE_NODELIST   = 101
    TYPE_NODEJOIN   = 102
    TYPE_NODELEFT   = 103
    TYPE_TALKSTART  = 104
    TYPE_TALKSTOP   = 105
    TYPE_TGSELECT   = 106
    TYPE_TGMONITOR  = 107
    TYPE_NODEINFO   = 111

    TYPE_AUDIO      = 101
    TYPE_FLUSH      = 102
    TYPE_FLUSHED    = 103
)

// ------------- general functions

func pack_string(b []byte, s string) int {
    l := len(s)
    binary.BigEndian.PutUint16(b, uint16(l))
    copy(b[2:], []byte(s))
    return 2 + l
}

func pack_array(b []byte, s []byte) int {
    l := len(s)
    binary.BigEndian.PutUint16(b, uint16(l))
    copy(b[2:], s)
    return 2 + l
}

// -------------------- packages structs

// -------------------- AUTHCHAL
type PktAUTHCHAL struct {
    DigSize uint16
    Dig []byte
}

func (p *PktAUTHCHAL) Unpack(b []byte) bool {
    // digsize(2) dig(20)
    if len(b) < 2 {
        return false
    }
    lenb := uint16(len(b))
    p.DigSize = binary.BigEndian.Uint16(b[:2])
    if (2 + p.DigSize) > lenb {
        return false
    }
    p.Dig = make([]byte, 20)
    copy(p.Dig, b[2:])
    return true
}

// ------------------- AUTHRESP
type PktAUTHRESP struct {
    CallSign string
    Dig []byte
}

func (p *PktAUTHRESP) Pack() []byte {
    tlen := 30 + len(p.CallSign)
    r := make([]byte, tlen)
    s := 0
    binary.BigEndian.PutUint32(r, uint32(tlen - 4))
    s += 4
    binary.BigEndian.PutUint16(r[s:], TYPE_AUTHRESP)
    s += 2
    s += pack_string(r[s:], p.CallSign)
    s += pack_array(r[s:], p.Dig)
    return r[:s]
}

// --------------------- ERROR
type PktERROR struct {
    Str string
}

func (p *PktERROR) Unpack(b []byte) bool {
    if len(b) < 2 {
        return false
    }
    l := binary.BigEndian.Uint16(b[:2])
    if int(2 + l) > len(b) {
        return false
    }
    p.Str = string(b[2:2+l])
    return true
}

// ----------------------- SERVINFO
type PktSERVINFO struct {
    ClientID uint32
    Nodes []string
    Codecs []string
}

func (p *PktSERVINFO) Unpack(b []byte) bool {
    if len(b) < 4 {
        return false
    }
    p.ClientID = binary.BigEndian.Uint32(b[:4])
    //n := binary.BigEndian.Uint16(b[4:6])
    // rest is not implemented, I don't care atm
    return true
}

// ------------------------- TGSELECT
type PktTGSELECT struct {
    TG uint32
}

func (p *PktTGSELECT) Pack() []byte {
    r := make([]byte, 10)
    s := 0
    binary.BigEndian.PutUint32(r, uint32(6))
    s += 4
    binary.BigEndian.PutUint16(r[s:], TYPE_TGSELECT)
    s += 2
    binary.BigEndian.PutUint32(r[s:], uint32(p.TG))
    s += 4
    return r[:s]
}

// ------------------------- TGMONITOR
type PktTGMONITOR struct {
    TGs []uint32
}

func (p *PktTGMONITOR) Pack() []byte {
    r := make([]byte, 10 + (2*len(p.TGs)))
    s := 0
    binary.BigEndian.PutUint32(r, uint32(8))
    s += 4
    binary.BigEndian.PutUint16(r[s:], TYPE_TGMONITOR)
    s += 2
    binary.BigEndian.PutUint16(r[s:], uint16(len(p.TGs)))
    s += 2
    for _, tg := range p.TGs {
        binary.BigEndian.PutUint32(r[s:], uint32(tg))
        s += 4
    }
    return r[:s]
}

// -------------------------------- TALKSTART, TALKSTOP
type PktTALKST struct {
    StartStop uint16
    TG uint32
    Callsign string
}

func (p *PktTALKST) Pack() []byte {
    r := make([]byte, 12 + len(p.Callsign))
    s := 0
    binary.BigEndian.PutUint32(r, uint32(8 + len(p.Callsign)))
    s += 4
    binary.BigEndian.PutUint16(r[s:], p.StartStop)
    s += 2
    binary.BigEndian.PutUint32(r[s:], p.TG)
    s += 4
    s += pack_string(r[s:], p.Callsign)
    return r[:s]
}

func (p *PktTALKST) Unpack(b []byte) bool{
    if len(b) < 6 {
        return false
    }
    p.TG = binary.BigEndian.Uint32(b[:4])
    l := binary.BigEndian.Uint16(b[4:6])
    if int(l + 6) > len(b) {
        return false
    }
    p.Callsign = string(b[6:6+l])
    return true
}

// ------------------------------- NODEINFO

type PktNODEINFO struct {
    Json string
}

func (p *PktNODEINFO) Pack() []byte {
    r := make([]byte, len(p.Json) + 8)
    s := 0
    binary.BigEndian.PutUint32(r, uint32(len(p.Json) + 4))
    s += 4
    binary.BigEndian.PutUint16(r[s:], TYPE_NODEINFO)
    s += 2
    s += pack_string(r[s:], p.Json)
    return r
}

// ----------------------------------- AUDIO
type PktAUDIO struct {
    ClientID uint32
    Seq uint16
    Data []byte
}

func (p *PktAUDIO) Unpack(b []byte) bool {
    if len(b) < 4 {
        return false
    }
    // b[0:2] is ClientID
    p.Seq = binary.BigEndian.Uint16(b[2:4])
    l := binary.BigEndian.Uint16(b[4:6])
    if l == 0 || (l != uint16(len(b[6:]))) {
        return false
    }
    p.Data = make([]byte, l)
    copy(p.Data, b[6:])
    return true
}

func (p *PktAUDIO) Pack() []byte {
    r := make([]byte, 1024)
    s := 0
    binary.BigEndian.PutUint16(r[s:], TYPE_AUDIO)
    s += 2
    binary.BigEndian.PutUint16(r[s:], uint16(p.ClientID))
    s += 2
    binary.BigEndian.PutUint16(r[s:], p.Seq)
    s += 2
    binary.BigEndian.PutUint16(r[s:], uint16(len(p.Data)))
    s += 2
    copy(r[s:], p.Data)
    s += len(p.Data)
    return r[:s]
}

// ------------------ SVXLink
type SVXLink struct {
    // written once, read many times
    admin_conn, voice_conn net.Conn
    callsign, password string
    ClientID uint32
    rxc, txc chan []byte

    // constantly read/written
    busy bool // someone talks (reflector side)
    tx bool   // I am transmitting to reflector
    Count uint16
    SelectedTG uint32
    WantedTG uint32

    mux sync.Mutex
}

func (s *SVXLink) tx_status_on() {
    if s.admin_conn == nil {
        return
    }
    s.admin_conn.Write([]byte{0, 0, 0, 6, 0, 0x71, 0, 1, 0x54, 1})
}

func (s *SVXLink) tx_status_off() {
    if s.admin_conn == nil {
        return
    }
    s.admin_conn.Write([]byte{0, 0, 0, 6, 0, 0x71, 0, 1, 0x54, 0})
}


func (s *SVXLink) send_proto_version() {
    // all data is network byte order (big endian)
    // len is 6 (4B), ptype is 5, maj version is 1, min version is 0 (2B)
    s.admin_conn.Write([]byte{0, 0, 0, 6, 0, 5, 0, 2, 0, 0})
}

func (s *SVXLink) send_admin_heartbeat() {
    // hardcoded
    if s.admin_conn == nil {
        return
    }
    s.admin_conn.Write([]byte{0, 0, 0, 2, 0, 1})
}

func (s *SVXLink) send_voice_flush_hb(flushhb uint16) {
    if s.voice_conn == nil {
        return
    }
    b := make([]byte, 6)
    binary.BigEndian.PutUint16(b, flushhb)
    binary.BigEndian.PutUint16(b[2:], uint16(s.ClientID))
    binary.BigEndian.PutUint16(b[4:], s.Count)
    s.mux.Lock()
    s.Count++
    s.mux.Unlock()
    s.voice_conn.Write(b)
}

func (s *SVXLink) send_voice_heartbeat() {
    s.send_voice_flush_hb(TYPE_HEARTBEAT)
}

func (s *SVXLink) send_flush() {
    s.send_voice_flush_hb(TYPE_FLUSH)
}

func (s *SVXLink) send_talk_startstop(callsign string, tg uint32,
    startstop uint16) {
    p := &PktTALKST{
        StartStop: startstop,
        TG: tg,
        Callsign: callsign,
    }
    if s.admin_conn == nil {
        return
    }
    s.admin_conn.Write(p.Pack())
}

func (s *SVXLink) send_talk_start(callsign string, tg uint32) {
    s.send_talk_startstop(callsign, tg, TYPE_TALKSTART)
}

func (s *SVXLink) send_talk_stop(callsign string, tg uint32) {
    s.send_talk_startstop(callsign, tg, TYPE_TALKSTOP)
}

func (s *SVXLink) Init(server, callsign, password, tg string,
    rxc, txc chan []byte) bool {
    // admin channel
    s.callsign = callsign
    s.password = password
    tguint, err := strconv.ParseUint(tg, 10, 32)
    if err != nil {
        fmt.Fprintf(os.Stderr, "SVXLink: cannot convert TG '%s' to uint32\n",
            tg)
        return false
    }
    s.WantedTG = uint32(tguint)
    s.rxc = rxc
    s.txc = txc
    for {
        s.admin_conn, err = net.Dial("tcp", server)
        if err != nil {
            fmt.Fprintf(os.Stderr, "SVXLink: %s\n", err.Error())
            s.admin_conn = nil
            time.Sleep(5 * time.Second)
            continue
        }
        break
    }
    fmt.Fprintf(os.Stderr, "SVXLink: %s connected\n", server)
    s.send_proto_version()
    // tcp ping goroutine
    go func(){
        for {
            s.send_admin_heartbeat()
            time.Sleep(10 * time.Second)
        }
    }()
    // admin goroutine
    go func(){
        for {
            b := make([]byte, 1024)
            r, err := s.admin_conn.Read(b)
            //fmt.Printf("%d\n", r)
            if err != nil {
                if err.Error() == "EOF" {
                    fmt.Fprintf(os.Stderr,
                        "SVXLink: disconnected. Reconnecting ..\n")
                    s.admin_conn.Close()
                    for {
                        s.busy = false
                        s.tx = false
                        s.Count = 0
                        s.SelectedTG = 0
                        s.WantedTG = uint32(tguint)
                        s.admin_conn, err = net.Dial("tcp", server)
                        if err != nil {
                            fmt.Fprintf(os.Stderr, "SVXLink: %s\n", err.Error())
                            time.Sleep(5 * time.Second)
                            continue
                        }
                        s.send_proto_version()
                        break
                    }
                } else {
                    fmt.Fprintf(os.Stderr, "SVXLink: %s\n", err.Error())
                }
                continue
            }
            if r == 0 {
                continue
            }
            s.admin_packet(b[:r])
        }
    }()
    // voice channel
    s.voice_conn, err = net.Dial("udp", server)
    if err != nil {
        fmt.Fprintf(os.Stderr, "SVXLink: %s\n", err.Error())
        return false
    }
    // udp ping goroutine - first packages will be wrong (who cares)
    go func(){
        for {
            s.send_voice_heartbeat()
            time.Sleep(10 * time.Second)
        }
    }()
    // voice goroutine
    go func(){
        for {
            b := make([]byte, 1024)

            r, err := s.voice_conn.Read(b)
            if err != nil {
                fmt.Fprintf(os.Stderr, "SVXLink: %s\n", err.Error())
                continue
            }
            //fmt.Printf("%d\n", r)
            if r == 0 {
                continue
            }
            s.voice_packet(b[:r])
        }
    }()
    go s.voice_tx()
    return true
}

func (s *SVXLink) admin_packet(b []byte) {
    /*
        Format: [4 bytes Big Endian][2 bytes BE]...
                ^^^^^^^^^^^^^^^^^^^ ^^^^^^^^^^^
                |                   +- type
                +- length
    */
    if len(b) < 6 {
        fmt.Fprintf(os.Stderr, "len(b) < 6\n")
        return
    }
    length := binary.BigEndian.Uint32(b[:4])
    typ := binary.BigEndian.Uint16(b[4:6])
    if int(length + 4) > len(b) {
        fmt.Fprintf(os.Stderr, "length + 4 > len(b) [%d > %d]\n",
            length + 4, len(b))
        fmt.Fprintf(os.Stderr, "Typ: %d\n", typ)
        return
    }
    //fmt.Printf("l %d t %d\n", length, typ)
    switch typ {
        case TYPE_HEARTBEAT:
            //fmt.Printf("TYPE_HEARTBEAT\n")
        case TYPE_PROTOVER:
            fmt.Printf("TYPE_PROTOVER\n")
        case TYPE_AUTHCHAL:
            //fmt.Printf("TYPE_AUTHCHAL\n")
            p := PktAUTHCHAL{}
            if !p.Unpack(b[6:]) {
                fmt.Fprintf(os.Stderr, "AUTHCHAL failed to unpack()\n")
                return
            }
            m := hmac.New(sha1.New, []byte(s.password))
            m.Write(p.Dig)
            sum := m.Sum(nil)
            r := PktAUTHRESP{
                CallSign: s.callsign,
                Dig: sum,
            }
            s.admin_conn.Write(r.Pack())
        case TYPE_AUTHRESP:
            //fmt.Printf("TYPE_AUTHRESP\n")
        case TYPE_AUTHOK:
            fmt.Printf("TYPE_AUTHOK\n")
        case TYPE_ERROR:
            //fmt.Printf("TYPE_ERROR\n")
            e := PktERROR{}
            if !e.Unpack(b[6:]) {
                fmt.Fprintf(os.Stderr, "ERROR failed to unpack()\n")
                return
            }
            fmt.Fprintf(os.Stderr, "SVXLink: ERROR: %s\n", e.Str)
        case TYPE_SERVINFO:
            //fmt.Printf("TYPE_SERVINFO\n")
            si := PktSERVINFO{}
            if !si.Unpack(b[6:]) {
                fmt.Fprintf(os.Stderr, "SERVINFO failed to unpack()\n")
                return
            }
            s.ClientID = si.ClientID
            fmt.Fprintf(os.Stderr, "SVXLink: ClientID 0x%x\n", si.ClientID)
            s.send_voice_heartbeat()
            ni := PktNODEINFO{
                Json: `{"sw":"SvxLink","swVer":"1.7.99.20"}`,
            }
            s.admin_conn.Write(ni.Pack())
            tg := PktTGMONITOR{
                TGs: []uint32{s.WantedTG},
            }
            s.admin_conn.Write(tg.Pack())
        case TYPE_NODELIST:
            fmt.Printf("TYPE_NODELIST\n")
        case TYPE_NODEJOIN:
            //fmt.Printf("TYPE_NODEJOIN\n")
        case TYPE_NODELEFT:
            //fmt.Printf("TYPE_NODELEFT\n")
        case TYPE_TALKSTART:
            fmt.Printf("TYPE_TALKSTART\n")
            s.mux.Lock()
            s.busy = true
            s.mux.Unlock()
            ts := PktTALKST{}
            if !ts.Unpack(b[6:]) {
                fmt.Fprintf(os.Stderr, "TALKST failed to unpack()\n")
                return
            }
            if s.SelectedTG == 0 {
                tgs := PktTGSELECT{
                    TG: ts.TG,
                }
                s.admin_conn.Write(tgs.Pack())
                s.SelectedTG = ts.TG
            }
            fmt.Fprintf(os.Stderr, "talkstart %s on %d\n", ts.Callsign, ts.TG)
            //s.tx_status_on()
        case TYPE_TALKSTOP:
            fmt.Printf("TYPE_TALKSTOP\n")
            s.mux.Lock()
            s.busy = false
            s.mux.Unlock()
            ts := PktTALKST{}
            if !ts.Unpack(b[6:]) {
                fmt.Fprintf(os.Stderr, "TALKST failed to unpack() [2]\n")
                return
            }
            fmt.Fprintf(os.Stderr, "talkstop %s on %d\n", ts.Callsign, ts.TG)
            //s.tx_status_off()
        default:
            fmt.Fprintf(os.Stderr, "SVXLink: unknown admin packet %d\n", typ)
    }
    if int(length + 4) < len(b) {
        // return (length + 4 + admin_packet(b[length+4:]))
        s.admin_packet(b[length+4:])
    }
}

func (s *SVXLink) voice_packet(b []byte) {
    if len(b) < 2 {
        return
    }
    typ := binary.BigEndian.Uint16(b[:2])
    switch typ {
        case TYPE_HEARTBEAT:
            //fmt.Printf("UDP HB\n")
        case TYPE_AUDIO:
            //fmt.Printf("AUDIO\n")
            pa := PktAUDIO{}
            if !pa.Unpack(b[2:]) {
                return
            }
            if s.rxc != nil {
                s.rxc <- pa.Data
            }
        case TYPE_FLUSH:
            fmt.Printf("FLUSH\n")
        case TYPE_FLUSHED:
            fmt.Printf("FLUSHED\n")
        default:
            fmt.Fprintf(os.Stderr, "SVXLink: unknown voice packet %d\n", typ)
    }
}

func (s *SVXLink) voice_tx(){
    // tx/rx managing routine
    var last time.Time
    var mux sync.Mutex
    go func(){
        for {
            // set to tx
            for {
                // if timeout break
                time.Sleep(10 * time.Millisecond)
                mux.Lock()
                l := last
                mux.Unlock()
                if time.Now().Sub(l) < 20 * time.Millisecond {
                    break
                }
            }
            s.mux.Lock()
            if s.SelectedTG == 0 {
                tgs := PktTGSELECT{
                    TG: s.WantedTG,
                }
                s.admin_conn.Write(tgs.Pack())
                s.SelectedTG = s.WantedTG
            }
            s.mux.Unlock()
            s.tx_status_on()
            fmt.Fprintf(os.Stderr, "SVXLink: TX\n")
            s.tx = true
            for {
                // if timeout break
                time.Sleep(50 * time.Millisecond)
                mux.Lock()
                l := last
                mux.Unlock()
                if time.Now().Sub(l) > 500 * time.Millisecond {
                    break
                }
            }
            s.tx = false
            fmt.Fprintf(os.Stderr, "SVXLink: RX\n")
            s.send_flush()
            s.tx_status_off()
        }
    }()
    for {
        if s.txc == nil {
            fmt.Fprintf(os.Stderr, "SVXLink: txc is nil, disabling tx\n")
            break
        }
        f := <-s.txc

        mux.Lock()
        last = time.Now()
        mux.Unlock()

        txa := PktAUDIO{
            ClientID: s.ClientID,
            Seq: s.Count,
            Data: f,
        }
        s.mux.Lock()
        s.Count++
        s.mux.Unlock()
        s.voice_conn.Write(txa.Pack())
    }
}

