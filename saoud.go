package main

import (
    "bufio"
    "bytes"
    "flag"
    "fmt"
    "math"
    "net"
    "os"
    "time"
    "strings"
    "sync"
    "math/rand"
    "encoding/binary"
    "crypto/sha256"

    "github.com/golang/protobuf/proto"
    "gitlab.com/galberti/gopus"
    "gitlab.com/galberti/saoud/svxlink"
    "gitlab.com/galberti/saoud/saoudproto"
)

const (
    CLIENT_NEW = iota
    CLIENT_AUTH
    CLIENT_PING
    SERVER_NEW
    SERVER_SENT_AUTH
    SERVER_AUTH
)

const (
    CODEC_OPUS = iota
)

type Opus struct {
    E *gopus.Encoder
    D *gopus.Decoder
}

func (p *Opus) SetBitrate(br int) {
    p.E.SetBitrate(br)
}

const OPUS_BITRATE = 48000
func (p *Opus) init(br int) {
    var e error
    p.E, e = gopus.NewEncoder(OPUS_BITRATE, 1, gopus.Voip)
    if e != nil {
        panic(e)
    }
    p.E.SetBitrate(br)
    p.D, e = gopus.NewDecoder(OPUS_BITRATE, 1)
    if e != nil {
        panic(e)
    }
}

func (p *Opus) Encode(pcm []int16, frameSize int) ([]byte, error) {
    // 1276 is recommended for opus (to adapt if more codec supported)
    // https://www.opus-codec.org/docs/html_api/group__opusencoder.html
    return p.E.Encode(pcm, frameSize, 1276)
}

func (p *Opus) Decode(data []byte, frameSize int, fec bool) ([]int16, error) {
    return p.D.Decode(data, frameSize, fec)
}

var opcodec Opus
var soundback, localback, stdoutback *bool
var servermode *bool
var svxback *string
var host *string
var port *uint64
var packetloss *float64

type Client struct {
    Addr net.Addr
    Status uint8
    Challenge uint64
    LastHeard time.Time
    SeqNo uint32

    // only for clients, to check lost packets
    RecvSeq uint32
    LastSample int16
}

const MAX_CLIENTS = 10
var version = uint32(1)
var clients map[string]*Client
var server *Client
var mux sync.Mutex
var done chan bool
var pc net.PacketConn
var mc net.PacketConn
var mcaddr net.Addr
var mcast chan []byte

var playc chan *[]int16 = nil
var recc chan *[]int16 = nil
var svxrxc, svxtxc chan []byte
var audiotx bool = false

func hash(chall uint64, passw *string) [32]byte {
    sh := make([]byte, 8)
    binary.BigEndian.PutUint64(sh, chall)
    sh = append(sh, []byte(*passw)...)
    return sha256.Sum256(sh)
}

// this produces the final wrapper
func get_packet(typ string, pb *[]byte) []byte {
    p := &saoudproto.Packet{}
    t := saoudproto.PacketPtype_value[typ]
    p.PType = saoudproto.PacketPtype(t).Enum()
    if pb != nil {
        p.Packet = *pb
    }
    pm, err := proto.Marshal(p)
    if err != nil {
        fmt.Fprintf(os.Stderr, "sendpack: couldn't marshal\n")
        return nil
    }
    return pm
}

var conn_tone, disc_tone []byte
func generate_tones(){
    conn_freq := 1600.0
    disc_freq := 600.0
    gs := int(48000 * 2);
    //conn_tone = make([]byte, gs*2)
    //disc_tone = make([]byte, gs*2)
    c_tone16 := make([]int16, gs)
    d_tone16 := make([]int16, gs)
    for i := 0; i < gs; i++ {
        // 32767 is max, let's stay a bit lower
        c_tone16[i] = int16(32767 * 0.2 * math.Sin(
            2.0 * math.Pi * conn_freq * float64(i)/48000.0))
        d_tone16[i] = int16(32767 * 0.2 * math.Sin(
            2.0 * math.Pi * disc_freq * float64(i)/48000.0))
    }
    bbuf_c := bytes.Buffer{}
    bbuf_d := bytes.Buffer{}
    binary.Write(&bbuf_c, binary.BigEndian, c_tone16)
    conn_tone = bbuf_c.Bytes()
    binary.Write(&bbuf_d, binary.BigEndian, d_tone16)
    disc_tone = bbuf_d.Bytes()
}

func multicast(pb *[]byte) {
    // I can't use sendpack - different socket
    f := 0
    d := 960
    tot := len(*pb)
    if d > tot {
        d = tot
    }
    for {
        npb := (*pb)[f:f+d]
        pm := get_packet("PCM", &npb)
        if pm == nil {
            return
        }
        mc.WriteTo(pm, mcaddr)
        if f + d == tot {
            break
        }
        f = f + d
        if f + d > tot {
            d = tot - f
        }
    }
}

// buffer in milliseconds
const buf_ms int = 1200
func multicast_mgr(b chan []byte){
    // here 1000 is the numner of _slices_ (1 slice = 1 packets)
    // so it's 10s @10ms framesize
    buffer := make(chan []byte, 1000)
    var bef time.Time
    for {
        lenb := 0
        var lenbmax, lenbmin float32
        waitdata := true
        for waitdata {
            select {
                case d := <-b:
                    // how many ms is this packet? ex. 960 000/48000/2 = 10
                    if lenb == 0 {
                        lenb = len(d)*1000/OPUS_BITRATE/2
                        lenbmax = float32(buf_ms) / float32(lenb) * 1.2
                        lenbmin = float32(buf_ms) / float32(lenb) * 0.4
                    }
                    buffer <- d
                    bef = time.Now()
                    // filled, exit
                    if len(buffer) == (buf_ms/lenb) {
                        waitdata = false
                        break
                    }
                case <- time.After(10 * time.Millisecond):
                    // discard old uncompleted data
                    if (time.Now().Sub(bef) > time.Duration(
                        (float32(buf_ms) * 1.5)) * time.Millisecond) &&
                        len(buffer) > 0 {
                        buffer = make(chan []byte, 1000)
                        lenb = 0
                    }
            }
        }
        havedata := true
        bef = time.Now()
        // init at 10ms is too much - 9.7 seems to be a good value
        ddelay := time.Duration(lenb * 970) * time.Microsecond
        ddmax := time.Duration(lenb * 990) * time.Microsecond
        ddmin := time.Duration(lenb * 920) * time.Microsecond
        hyst := 0
        for havedata {
            select {
                case d := <-b:
                    hyst++
                    lbuf := len(buffer)
                    if hyst == 10 {
                        // manage variable wait time every 10 packets
                        if (lbuf > int(lenbmax)) && (ddelay > ddmin) {
                            ddelay = ddelay - time.Duration(
                                100 * time.Microsecond)
                        } else if (lbuf < int(lenbmin)) && (ddelay < ddmax) {
                            ddelay = ddelay + time.Duration(
                                100 * time.Microsecond)
                        }
                        hyst = 0
                    }
                    // check overruns
                    if lbuf > 980 {
                        fmt.Fprintf(os.Stderr, "multicast buffer overrun\n")
                    } else {
                        buffer <- d
                    }
                case <-time.After(100 * time.Microsecond):
                    if time.Now().Sub(bef) >= ddelay {
                        if len(buffer) == 0 {
                            havedata = false
                            break
                        } else {
                            bef = time.Now()
                            d := <-buffer
                            multicast(&d)
                        }
                    }
            }
        }
    }
}

func sendpack(typ string, pb *[]byte, addr net.Addr, reps int) {
    // FIXME do crypto
    pm := get_packet(typ, pb)
    if pm == nil {
        return
    }
    for i := 0; i < reps; i++ {
        pc.WriteTo(pm, addr)
    }
}

func serve(addr net.Addr, buf []byte, passw *string, br *uint64) {
    p := &saoudproto.Packet{}
    if err := proto.Unmarshal(buf, p); err != nil {
        fmt.Fprintf(os.Stderr, "couldn't unmarshal (1)\n")
        fmt.Fprintf(os.Stderr, "%v\n", buf)
        return
    }
    switch *p.PType {
        case saoudproto.Packet_PING:
            var c *Client
            if *servermode {
                mux.Lock()
                defer mux.Unlock()
                var ok bool
                c, ok = clients[addr.String()]
                if !ok {
                    return
                }
            } else {
                c = server
            }
            c.LastHeard = time.Now()
        case saoudproto.Packet_AREP:
            mux.Lock()
            defer mux.Unlock()
            if server == nil || server.Status != SERVER_NEW {
                return
            }
            sbi := &saoudproto.ARep{}
            if err := proto.Unmarshal(p.Packet, sbi); err != nil {
                fmt.Fprintf(os.Stderr, "couldn't unmarshal (2)\n")
                return
            }
            lh := hash(*sbi.Challenge, passw)
            sp := &saoudproto.Auth{
                Version: &version,
                Hash: lh[:],
            }
            spm, err := proto.Marshal(sp)
            if err != nil {
                fmt.Fprintf(os.Stderr, "couldn't marshal (1)\n")
                return
            }
            sendpack("AUTH", &spm, server.Addr, 3)
            server.Status = SERVER_SENT_AUTH
        case saoudproto.Packet_AUTHOK:
            mux.Lock()
            defer mux.Unlock()
            if server == nil || server.Status != SERVER_SENT_AUTH {
                return
            }
            fmt.Fprintf(os.Stderr, "Auth OK\n")
            done <- true
            server.Status = SERVER_AUTH
            server.LastHeard = time.Now()
            if *localback {
                multicast(&conn_tone)
            }
            aok := &saoudproto.AuthOK{}
            if err := proto.Unmarshal(p.Packet, aok); err != nil {
                fmt.Fprintf(os.Stderr, "couldn't unmarshal (3)\n")
                return
            }
            if *soundback {
                fmt.Fprintf(os.Stderr, "bitrate set by server to %d\n",
                    int(*aok.Bitrate))
                opcodec.SetBitrate(int(*aok.Bitrate))
            }
        case saoudproto.Packet_AUTHNOK:
            fmt.Fprintf(os.Stderr, "Auth failed.\n")
            os.Exit(1)
        case saoudproto.Packet_AREQ:
            if len(clients) < MAX_CLIENTS {
                mux.Lock()
                defer mux.Unlock()
                _, ok := clients[addr.String()]
                if ok {
                    return
                }
                c := Client{
                    Addr: addr,
                    Status: CLIENT_NEW,
                    Challenge: rand.Uint64(),
                    LastHeard: time.Now(),
                    SeqNo: 0,
                }
                clients[addr.String()] = &c
                pr := &saoudproto.ARep{
                    Challenge: &c.Challenge,
                }
                mpr, e := proto.Marshal(pr)
                if e != nil {
                    fmt.Fprintf(os.Stderr, "couldn't marshal (2)\n")
                    return
                }
                sendpack("AREP", &mpr, addr, 3)
            } else {
                fmt.Fprintf(os.Stderr, "warning: clients map is full")
            }
        case saoudproto.Packet_AUTH:
            mux.Lock()
            defer mux.Unlock()
            c, ok := clients[addr.String()]
            if !ok {
                return
            }
            if c.Status != CLIENT_NEW {
                return
            }
            lh := hash(c.Challenge, passw)
            sb := &saoudproto.Auth{}
            if err := proto.Unmarshal(p.Packet, sb); err != nil {
                fmt.Fprintf(os.Stderr, "couldn't unmarshal (4)\n")
                return
            }
            var ty string
            if bytes.Compare(sb.Hash, lh[:]) != 0 {
                ty = "AUTHNOK"
                delete(clients, addr.String())
                sendpack(ty, nil, addr, 3)
            } else {
                ty = "AUTHOK"
                pak := &saoudproto.AuthOK{
                    Bitrate: br,
                }
                pakm, e := proto.Marshal(pak)
                if e != nil {
                    fmt.Fprintf(os.Stderr, "couldn't marshal (3)\n")
                    return
                }
                c.Status = CLIENT_AUTH
                c.LastHeard = time.Now()
                sendpack(ty, &pakm, addr, 3)
            }
        case saoudproto.Packet_VOICE:
            var c *Client
            mux.Lock()
            defer mux.Unlock()
            if *servermode {
                var ok bool
                c, ok = clients[addr.String()]
                if !ok {
                    return
                }
            } else {
                c = server
            }
            if c.Status != CLIENT_AUTH && c.Status != SERVER_AUTH {
                return
            }

            sb := &saoudproto.Voice{}
            if err := proto.Unmarshal(p.Packet, sb); err != nil {
                fmt.Fprintf(os.Stderr, "couldn't unmarshal (5)\n")
                return
            }
            if *servermode {
                go send_voice(sb.Data, &addr)
            } else {
                if *svxback != "" {
                    // send to svxlink
                    svxtxc <- sb.Data
                } else {
                    // discard out of order packs (ex 1 2 4 3 -> discard 3)
                    // 2 scenarios, 4 cases.
                    //   p1 arrives, p2 arrives after. 0 is where uint32 wraps.
                    // - scenario 1
                    //           p2b .... p1 .... 0 .... p2a
                    //   p2a:
                    //     int(p2a) - int(p1) < 0 and close to max  (fill)
                    //       u(p2a) -   u(p1) > 0 and close to 0    (fill)
                    //   p2b: int(p2b) - int(p1) < 0 and close to 0 (discard)
                    // - scenario 2
                    //           0 .... p2b .... p1 .... p2a
                    //   p2a:
                    //     int(p2a) - int(p1) > 0 and close to 0    (fill)
                    //       u(p2a) -   u(p1) > 0 and close to 0    (fill)
                    //   p2b: int(p2b) - int(p1) < 0 and close to 0 (discard)
                    //
                    ni := int(*sb.SeqNo) - int(server.RecvSeq)
                    // both discards above (max 15 packs lost)
                    if (ni < 0) && (ni > -15) {
                        return
                    }
                    // cut for packet loss
                    if *packetloss != 0 && (rand.Float64() < *packetloss) {
                        fmt.Fprintf(os.Stderr, "losing packet %d\n", *sb.SeqNo)
                        return
                    }
                    // this handles up to 100 ms per packet ..
                    sdec, e := opcodec.Decode(sb.Data, OPUS_BITRATE/10, false)
                    if e != nil {
                        fmt.Fprintf(os.Stderr, "opus decode: %s\n", e.Error())
                        return
                    }
                    // fills with ints change sign, so I use unsigned (casted)
                    n32 := int(*sb.SeqNo - server.RecvSeq)
                    // limit max lost packets
                    // n32 can be up to 4.3e9, ~90k sec @48ks/s!
                    if (n32 > 0) && (n32 < 20) {
                        lensdec := len(sdec)
                        fill := make([]int16, lensdec)
                        diff := float32(sdec[0] - server.LastSample) /
                            float32(lensdec * n32)
                        for i := 0; i < n32; i++ {
                            // generate fill package
                            for j := 0; j < lensdec; j++ {
                                fill[j] = server.LastSample +
                                    int16(diff * float32(j + i*lensdec))
                            }
                            if *soundback {
                                playc <- &fill
                            }
                            if *localback || *stdoutback {
                                bbuf := bytes.Buffer{}
                                binary.Write(&bbuf, binary.BigEndian, fill)
                                buf := bbuf.Bytes()
                                if *localback {
                                    mcast <- buf
                                    //multicast(&buf)
                                }
                                if *stdoutback {
                                    os.Stdout.Write(buf)
                                }
                            }
                        }
                        server.RecvSeq = *sb.SeqNo + 1
                    } else {
                        server.RecvSeq++
                    }
                    server.LastSample = sdec[len(sdec)-1]

                    if *soundback {
                        playc <- &sdec
                    }
                    if *localback || *stdoutback {
                        max := int16(0)
                        for _, v := range sdec {
                            if v > max {
                                max = v
                                continue
                            }
                            if -v > max {
                                max = -v
                            }
                        }
                        if max > 32760 {
                            fmt.Fprintf(os.Stderr, "max: %d\n", max)
                        }

                        bbuf := bytes.Buffer{}
                        binary.Write(&bbuf, binary.BigEndian, sdec)
                        buf := bbuf.Bytes()
                        if *localback {
                            mcast <- buf
                            //multicast(&buf)
                        }
                        if *stdoutback {
                            os.Stdout.Write(buf)
                        }
                    }
                }
            }
    }
}

func ping() {
    // a ping every 5 secs, timout after 15 secs
    for {
        nc := make(map[string]*Client)
        now := time.Now()
        mux.Lock()
        if *servermode {
            for _, v := range clients {
                if now.Sub(v.LastHeard) > 15 * time.Second {
                    fmt.Fprintf(os.Stderr, "%v timed out\n", v.Addr.String())
                    continue
                }
                sendpack("PING", nil, v.Addr, 1)
                nc[v.Addr.String()] = v
            }
            clients = nc
        } else {
            if now.Sub(server.LastHeard) > 15 * time.Second {
                fmt.Fprintf(os.Stderr, "%v timed out\n", server.Addr.String())
                if *localback {
                    multicast(&disc_tone)
                }
                saddr, err := net.ResolveUDPAddr("udp",
                    fmt.Sprintf("%s:%d", *host, *port))
                if err != nil {
                    fmt.Fprintf(os.Stderr, "dns error .. no network?\n")
                } else {
                    server.Addr = saddr
                }
                server.Status = SERVER_NEW
                server.SeqNo = 0
            } else {
                if server.Status == SERVER_AUTH {
                    sendpack("PING", nil, server.Addr, 1)
                }
            }
        }
        mux.Unlock()
        time.Sleep(5 * time.Second)
    }
}

func svx_to_server() {
    for {
        d := <-svxrxc
        mux.Lock()
        p := &saoudproto.Voice{
            SeqNo: &server.SeqNo,
            Data: d,
        }
        server.SeqNo++
        mux.Unlock()
        pm, _ := proto.Marshal(p)
        sendpack("VOICE", &pm, server.Addr, 1)
    }
}

func send_voice(buf []byte, excl *net.Addr) {
    mux.Lock()
    if *servermode {
        for _, c := range clients {
            if excl != nil && (*excl).String() == c.Addr.String() {
                continue
            }
            p := &saoudproto.Voice{
                SeqNo: &c.SeqNo,
                Data: buf,
            }
            c.SeqNo++
            pm, _ := proto.Marshal(p)
            sendpack("VOICE", &pm, c.Addr, 1)
        }
    } else {
        p := &saoudproto.Voice{
            SeqNo: &server.SeqNo,
            Data: buf,
        }
        server.SeqNo++
        pm, _ := proto.Marshal(p)
        sendpack("VOICE", &pm, server.Addr, 1)
    }
    mux.Unlock()
}

func rec_to_net() {
    for {
        s := <-recc
        if !audiotx {
            continue
        }
        senc, e := opcodec.Encode(*s, len(*s))
        if e != nil {
            fmt.Fprintf(os.Stderr, "opus encode (rec): %s\n", e.Error())
            continue
        }
        send_voice(senc, nil)
    }
}

func start_cli() {
    r := bufio.NewReader(os.Stdin)
    fmt.Fprintf(os.Stderr, "======> ready to read: enter toggles TX <======\n")
    for {
        t, _ := r.ReadString('\n')
        switch strings.Trim(t, "\n ") {
            case "":
                if audiotx {
                    fmt.Fprintf(os.Stderr, "RX\n")
                    audiotx = false
                } else {
                    fmt.Fprintf(os.Stderr, "TX\n")
                    audiotx = true
                }
            case "q":
                fmt.Fprintf(os.Stderr, "quit? CTRL-C..\n")
        }
    }
}

func start_audio() {
    sox_init()
    playc = make(chan *[]int16)
    recc = make(chan *[]int16)

    fmt.Fprintf(os.Stderr, "starting play goroutine\n")
    go play_loop(playc)

    fmt.Fprintf(os.Stderr, "starting rec goroutines\n")
    go rec_loop(recc)
    go rec_to_net()
}

func server_main(passw *string, br *uint64) {
    var err error
    pc, err = net.ListenPacket("udp", ":47308")
    if err != nil {
        panic(err)
    }
    defer pc.Close()

    fmt.Fprintf(os.Stderr, "launching ping handler\n")
    go ping()
    buf := make([]byte, 1024)
    fmt.Fprintf(os.Stderr, "entering main loop\n")
    for {
        n, addr, err := pc.ReadFrom(buf)
        //fmt.Fprintf(os.Stderr, "got %d\n", n)
        if err != nil {
            continue
        }
        serve(addr, buf[:n], passw, br)
    }
}

func doauth(c net.PacketConn, add net.Addr) {
    tmout := make(chan bool)
    go func() {
        time.Sleep(500 * time.Millisecond)
        tmout <- true
    }()
    for {
        select {
            case <- tmout:
                sendpack("AREQ", nil, add, 3)
                go func() {
                    time.Sleep(5 * time.Second)
                    tmout <- true
                }()
            case <- done:
            return
        }
    }
}

func auth(c net.PacketConn) {
    for {
        mux.Lock()
        s := server.Status
        mux.Unlock()
        if s != SERVER_AUTH {
            doauth(c, server.Addr)
        }
        time.Sleep(5 * time.Second)
    }
}

func serve_local(){
    var lptime time.Time
    var lpmux sync.Mutex
    confirm := false
    go func(){
        for {
            lpmux.Lock()
            l := lptime
            lpmux.Unlock()
            tslp := time.Now().Sub(l)
            if (tslp > (100 * time.Millisecond)) {
                if confirm {
                    multicast(&conn_tone)
                    confirm = false
                }
            } else {
                confirm = true
            }
            time.Sleep(10 * time.Millisecond)
        }
    }()
    buf := make([]byte, 1024)
    for {
        // FIXME manage multiplex
        n, addr, e := mc.ReadFrom(buf)
        if e != nil {
            continue
        }
        as := addr.String()
        // pkt *must* come from a private network. Not 100% safe but OKish
        if (as[:8] == "192.168.") || (as[:6] == "172.17") ||
            (as[:4] == "127.") || (as[:3] == "10.") {
            p := &saoudproto.Packet{}
            if err := proto.Unmarshal(buf[:n], p); err != nil {
                fmt.Fprintf(os.Stderr, "couldn't unmarshal mcast\n")
                fmt.Fprintf(os.Stderr, "%v\n", buf)
                continue
            }
            if *p.PType != saoudproto.Packet_PCM {
                continue
            }
            // convert []byte to []int16
            b16 := make([]int16, len(p.Packet)/2)
            bbuf := bytes.NewBuffer(p.Packet)
            binary.Read(bbuf, binary.LittleEndian, b16)
            senc, e := opcodec.Encode(b16, len(b16))
            if e != nil {
                fmt.Fprintf(os.Stderr, "opus encode (local): %s\n", e.Error())
                continue
            }
            lpmux.Lock()
            lptime = time.Now()
            lpmux.Unlock()
            send_voice(senc, nil)
        } else {
            fmt.Fprintf(os.Stderr, "PCM packet from %s\n", as)
        }
    }
}

func client_main(passw *string, host *string, port *uint64, bitrate *uint64) {
    var err error
    pc, err = net.ListenPacket("udp", ":0")
    if err != nil {
        panic(err)
    }
    saddr, err := net.ResolveUDPAddr("udp", fmt.Sprintf("%s:%d", *host, *port))
    if err != nil {
        panic(err)
    }
    server = &Client{
        Addr: saddr,
        Status: SERVER_NEW,
        Challenge: 0,
        LastHeard: time.Now(),
        SeqNo: 0,
    }
    done = make(chan bool)
    fmt.Fprintf(os.Stderr, "launching auth handler\n")
    go auth(pc)
    fmt.Fprintf(os.Stderr, "launching ping handler\n")
    go ping()

    if *svxback != "" {
        // auth svxlink, setup channels and svxlink routines
        s := svxlink.SVXLink{}
        pars := strings.Split(*svxback, ":")
        svxrxc = make(chan []byte)
        svxtxc = make(chan []byte)
        s.Init(pars[0] + ":" + pars[1], pars[2], pars[3], pars[4], svxrxc,
            svxtxc)
        go svx_to_server()
    } else {
        fmt.Fprintf(os.Stderr, "initialising opus\n")
        opcodec = Opus{}
        opcodec.init(int(*bitrate))
        if *soundback {
            start_audio()
            go start_cli()
        }
        if *localback {
            var err error
            mc, err = net.ListenPacket("udp", ":47308")
            if err != nil {
                panic(err)
            }
            mcaddr, err = net.ResolveUDPAddr("udp", "239.47.78.1:47781")
            if err != nil {
                panic(err)
            }
            go serve_local()
            mcast = make(chan []byte)
            go multicast_mgr(mcast)
        }
    }

    buf := make([]byte, 1024)
    fmt.Fprintf(os.Stderr, "entering main loop\n")
    for {
        n, addr, err := pc.ReadFrom(buf)
        if err != nil {
            continue
        }
        serve(addr, buf[:n], passw, nil)
        //fmt.Fprintf(os.Stderr, "%v\n", buf[:n])
    }
}

func main() {
    servermode = flag.Bool("s", false, "server mode")
    host = flag.String("h", "", "host for client mode")
    port = flag.Uint64("P", 47308, "server port")
    var bitrate = flag.Uint64("b", 16000, "bitrate for opus codec")
    var password = flag.String("p", "", "password to access")
    packetloss = flag.Float64("f", 0, "fake packet loss (ex: 0.1 for 10%)")

    soundback = flag.Bool("C", false, "soundcard backend for client mode")
    localback = flag.Bool("l", false, "local backend for client mode")
    stdoutback = flag.Bool("O", false, "print packets to stdout (s16)")
    svxback = flag.String("x", "", "svxlink server:port:user:passw:tg")
    flag.Parse()

    if *servermode == false && *host == "" {
        fmt.Fprintf(os.Stderr, "missing host\n")
        return
    }
    if *password == "" {
        fmt.Fprintf(os.Stderr, "missing password\n")
        return
    }
    if *localback {
        generate_tones()
    }

    clients = make(map[string]*Client)

    if *servermode == true {
        fmt.Fprintf(os.Stderr, "launching server\n")
        server_main(password, bitrate)
    } else {
        fmt.Fprintf(os.Stderr, "launching client\n")
        client_main(password, host, port, bitrate)
    }
}

